/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
DelaykataAudioProcessor::DelaykataAudioProcessor()
     : AudioProcessor(BusesProperties()
                       .withInput("Input",  AudioChannelSet::stereo(), true)
                       .withOutput("Output", AudioChannelSet::stereo(), true)
                       ),
    parameters(*this, nullptr, Identifier("DelayKata"), {
        std::make_unique<AudioParameterFloat>("time", "Time", 0, DelaykataAudioProcessor::maxTime, 0.5f),
        std::make_unique<AudioParameterFloat>("feedback", "Feedback", 0.0f, 100.0f, 33.3f),
        std::make_unique<AudioParameterFloat>("mix", "Mix", 0.0f, 100.0f, 50.0f)
    })
{
    timeParameter = parameters.getRawParameterValue("time");
    feedbackParameter = parameters.getRawParameterValue("feedback");
    mixParameter = parameters.getRawParameterValue("mix");
}

DelaykataAudioProcessor::~DelaykataAudioProcessor()
{
}

//==============================================================================
const String DelaykataAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool DelaykataAudioProcessor::acceptsMidi() const
{
    return false;
}

bool DelaykataAudioProcessor::producesMidi() const
{
    return false;
}

bool DelaykataAudioProcessor::isMidiEffect() const
{
    return false;
}

double DelaykataAudioProcessor::getTailLengthSeconds() const
{
    // TODO
    return 0.0;
}

int DelaykataAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int DelaykataAudioProcessor::getCurrentProgram()
{
    return 0;
}

void DelaykataAudioProcessor::setCurrentProgram(int index)
{
}

const String DelaykataAudioProcessor::getProgramName(int index)
{
    return {};
}

void DelaykataAudioProcessor::changeProgramName(int index, const String& newName)
{
}

//==============================================================================
void DelaykataAudioProcessor::prepareToPlay(double sampleRate, int samplesPerBlock)
{
    const int channels = getMainBusNumInputChannels();
    
    delayBuffers = new CircularBuffer*[channels];
    
    for (int i = 0; i < channels; i++)
        delayBuffers[i] = new CircularBuffer(ceil(sampleRate) * maxTime);
}

void DelaykataAudioProcessor::releaseResources()
{
    const int channels = getMainBusNumInputChannels();

    for (int i = 0; i < channels; i++)
        delete delayBuffers[i];
        
    delete delayBuffers;
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool DelaykataAudioProcessor::isBusesLayoutSupported(const BusesLayout& layouts) const
{
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    if(layouts.getMainOutputChannelSet() != AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
    if(layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;

    return true;
}
#endif

void DelaykataAudioProcessor::processBlock(AudioBuffer<float>& buffer, MidiBuffer& midiMessages)
{
    ScopedNoDenormals noDenormals;

    auto totalNumInputChannels  = getTotalNumInputChannels();
    auto totalNumOutputChannels = getTotalNumOutputChannels();
    
    float delayAmp = *mixParameter / 100;
    float feedbackAmp = *feedbackParameter / 100;
    
    for (auto i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear(i, 0, buffer.getNumSamples());

    for (int sample = 0; sample < buffer.getNumSamples(); ++sample) {
        for (int channel = 0; channel < totalNumInputChannels; ++channel) {
            auto* inputData = buffer.getReadPointer(channel);
            float inputSignal = inputData[sample];
            float delayedSignal = delayBuffers[channel]->get(*timeParameter * getSampleRate());
            
            auto* outputData = buffer.getWritePointer(channel);

            outputData[sample] = inputSignal + delayAmp * delayedSignal;
            
            delayBuffers[channel]->put(inputSignal + feedbackAmp * delayedSignal);
        }
    }
}

//==============================================================================
bool DelaykataAudioProcessor::hasEditor() const
{
    return true; //(change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* DelaykataAudioProcessor::createEditor()
{
    return new DelaykataAudioProcessorEditor(*this, parameters);
}

//==============================================================================
void DelaykataAudioProcessor::getStateInformation(MemoryBlock& destData)
{
    auto state = parameters.copyState();

    std::unique_ptr<XmlElement> xml(state.createXml());
    copyXmlToBinary(*xml, destData);
}

void DelaykataAudioProcessor::setStateInformation(const void* data, int sizeInBytes)
{
    std::unique_ptr<XmlElement> xmlState(getXmlFromBinary(data, sizeInBytes));
 
    if(xmlState.get() != nullptr)
        if(xmlState->hasTagName(parameters.state.getType()))
            parameters.replaceState(ValueTree::fromXml(*xmlState));
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new DelaykataAudioProcessor();
}
