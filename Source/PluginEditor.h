/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "PluginProcessor.h"

typedef AudioProcessorValueTreeState::SliderAttachment SliderAttachment;

//==============================================================================
/**
*/
class DelaykataAudioProcessorEditor  : public AudioProcessorEditor
{
public:
    DelaykataAudioProcessorEditor(DelaykataAudioProcessor&, AudioProcessorValueTreeState& state);
    ~DelaykataAudioProcessorEditor();

    //==============================================================================
    void paint(Graphics&) override;
    void resized() override;

private:
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    DelaykataAudioProcessor& processor;
    
    Slider timeSlider;
    Slider feedbackSlider;
    Slider mixSlider;
    
    Label timeLabel;
    Label feedbackLabel;
    Label mixLabel;
    
    std::unique_ptr<SliderAttachment> timeAttachment;
    std::unique_ptr<SliderAttachment> feedbackAttachment;
    std::unique_ptr<SliderAttachment> mixAttachment;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(DelaykataAudioProcessorEditor)
};
