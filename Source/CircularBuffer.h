//
//  CircularBuffer.h
//  Delay-Kata
//
//  Created by Patryk Miszczak on 22/03/2020.
//  Copyright © 2020 Patryk Miszczak. All rights reserved.
//

#ifndef CircularBuffer_h
#define CircularBuffer_h

#include <algorithm>
#include <JuceHeader.h>

class CircularBuffer {
    float* buffer;
    int mask;
    int writeIndex;
    
public:
    CircularBuffer(int size) :
        mask(nextPowerOfTwo(size) - 1),
        writeIndex(0)
    {
        jassert(size > 0);
        
        int samples = nextPowerOfTwo(size);
        
        buffer = new float[samples];
    
        std::fill(buffer, buffer + samples, 0);
    }
    
    ~CircularBuffer() {
        delete[] buffer;
    }
    
    inline void put(float input) {
        buffer[writeIndex & mask] = input;
        
        writeIndex = (writeIndex - 1) & mask;
    }
    
    inline float get(int delay) const {
        return buffer[writeIndex + delay & mask];
    }
};

#endif /* CircularBuffer_h */
