/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
DelaykataAudioProcessorEditor::DelaykataAudioProcessorEditor(DelaykataAudioProcessor& p, AudioProcessorValueTreeState& state)
    : AudioProcessorEditor(&p),
    processor(p),
    timeSlider(Slider::Rotary, Slider::NoTextBox),
    feedbackSlider(Slider::Rotary, Slider::NoTextBox),
    mixSlider(Slider::Rotary, Slider::NoTextBox)
{
    setSize(260, 120);

    timeSlider.setRange(0.01, DelaykataAudioProcessor::maxTime);
    timeSlider.setTextValueSuffix(" s");
    addAndMakeVisible(timeSlider);
    timeAttachment.reset(new SliderAttachment(state, "time", timeSlider));

    feedbackSlider.setRange(0, 100);
    feedbackSlider.setSkewFactorFromMidPoint(10);
    feedbackSlider.setTextValueSuffix("%");
    addAndMakeVisible(feedbackSlider);
    feedbackAttachment.reset(new SliderAttachment(state, "feedback", feedbackSlider));

    mixSlider.setRange(0, 100);
    mixSlider.setSkewFactorFromMidPoint(10);
    mixSlider.setTextValueSuffix("%");
    addAndMakeVisible(mixSlider);
    mixAttachment.reset(new SliderAttachment(state, "mix", mixSlider));
    
    timeLabel.setText("Time", dontSendNotification);
    timeLabel.setJustificationType(Justification::centred);
    addAndMakeVisible(timeLabel);
    
    feedbackLabel.setText("Feedback", dontSendNotification);
    feedbackLabel.setJustificationType(Justification::centred);
    addAndMakeVisible(feedbackLabel);
    
    mixLabel.setText("Mix", dontSendNotification);
    mixLabel.setJustificationType(Justification::centred);
    addAndMakeVisible(mixLabel);
}

DelaykataAudioProcessorEditor::~DelaykataAudioProcessorEditor()
{
}

//==============================================================================
void DelaykataAudioProcessorEditor::paint(Graphics& g)
{
    g.fillAll(getLookAndFeel().findColour(ResizableWindow::backgroundColourId));
}

void DelaykataAudioProcessorEditor::resized()
{
    timeSlider.setBounds(10, 10, 80, 80);
    timeLabel.setBounds(10, 90, 80, 20);
    feedbackSlider.setBounds(90, 10, 80, 80);
    feedbackLabel.setBounds(90, 90, 80, 20);
    mixSlider.setBounds(170, 10, 80, 80);
    mixLabel.setBounds(170, 90, 80, 20);
}
